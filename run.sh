#!/bin/bash

service nginx start
cd /home/ctf/mphone/ && sudo docker-compose up -d --build;
cd /home/ctf/askoracle/ && sudo docker-compose up -d --build;
cd /home/ctf/headhunter/container/ && sudo docker-compose up -d --build;
